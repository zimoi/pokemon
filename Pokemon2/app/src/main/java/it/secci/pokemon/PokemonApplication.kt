package it.secci.pokemon

import android.app.Application
import android.content.pm.ApplicationInfo
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class PokemonApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this

        if (isApplicationDebuggable()) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun isApplicationDebuggable(): Boolean =
        (applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE) != 0

    companion object {
        lateinit var instance: PokemonApplication
            private set
    }
}