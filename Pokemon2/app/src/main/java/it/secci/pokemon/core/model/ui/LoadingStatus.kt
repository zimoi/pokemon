package it.secci.pokemon.core.model.ui

enum class LoadingStatus {
    LOADING, OK, KO
}