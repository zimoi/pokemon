package it.secci.pokemon.core.network

import it.secci.pokemon.pokemondetails.model.network.PokemonDetailsNetworkModel
import it.secci.pokemon.pokemonlist.model.network.PokemonListResponseNetworkModel
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

const val PARAM_LIMIT : String = "limit"
const val PARAM_OFFSET : String = "offset"
const val LIMIT: Int = 20
const val OFFSET: Int = 0

interface NetworkService {

    @GET("pokemon")
    suspend fun getPokemonList(@Query(PARAM_LIMIT) limit: Int = LIMIT,
                               @Query(PARAM_OFFSET) offset: Int = OFFSET): PokemonListResponseNetworkModel

    @GET
    suspend fun getAnotherPage(@Url url: String?): PokemonListResponseNetworkModel

    @GET
    suspend fun getPokemonDetails(@Url url: String?): PokemonDetailsNetworkModel
}