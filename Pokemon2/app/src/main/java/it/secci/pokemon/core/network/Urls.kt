package it.secci.pokemon.core.network

object Urls {
    const val BASE_URL : String = "https://pokeapi.co/api/v2/"
}