package it.secci.pokemon.core.repository

import it.secci.pokemon.pokemondetails.model.network.PokemonDetailsNetworkModel
import it.secci.pokemon.pokemonlist.model.network.PokemonListResponseNetworkModel

interface Repository {
    suspend fun getPokemonList(): PokemonListResponseNetworkModel

    suspend fun downloadAnotherPage(pageUrl : String): PokemonListResponseNetworkModel

    suspend fun getPokemonDetails(pokemonDetailsUrl: String?): PokemonDetailsNetworkModel
}