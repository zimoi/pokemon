package it.secci.pokemon.core.repository

import it.secci.pokemon.core.network.NetworkService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RepositoryImpl @Inject constructor(private val networkService: NetworkService) :
    Repository {

    override suspend fun getPokemonList() = networkService.getPokemonList()

    override suspend fun downloadAnotherPage(pageUrl: String) =
        networkService.getAnotherPage(pageUrl)

    override suspend fun getPokemonDetails(pokemonDetailsUrl: String?) =
        networkService.getPokemonDetails(pokemonDetailsUrl)
}