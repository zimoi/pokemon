package it.secci.pokemon.pokemondetails.model.network

import com.squareup.moshi.Json
import it.secci.pokemon.pokemondetails.model.ui.PokemonDetailsPresentationModel

data class PokemonDetailsNetworkModel(
    @field:Json(name = "abilities") var abilities: List<Ability>,
    @field:Json(name = "count") var baseExperience: Long,
    @field:Json(name = "forms") var forms: List<Species>,
    @field:Json(name = "height") var height: Long,
    @field:Json(name = "id") var id: Long,
    @field:Json(name = "is_default") var isDefault: Boolean,
    @field:Json(name = "location_area_encounters") var locationAreaEncounters: String,
    @field:Json(name = "name") var name: String,
    @field:Json(name = "order") var order: Long,
    @field:Json(name = "species") var species: Species,
    @field:Json(name = "sprites") var sprites: Sprites,
    @field:Json(name = "weight") var weight: Long
)

data class Ability(
    @field:Json(name = "ability") var ability: Species,
    @field:Json(name = "is_hidden") var isHidden: Boolean,
    @field:Json(name = "slot") var slot: Long
)

data class Species(
    @field:Json(name = "name") var name: String,
    @field:Json(name = "url") var url: String
)

data class Sprites(
    @field:Json(name = "other") var other: Other
)

data class Other(
    @field:Json(name = "official-artwork") var officialArtwork: OfficialArtwork
)

data class OfficialArtwork(
    @field:Json(name = "front_default") var frontDefault: String
)

fun PokemonDetailsNetworkModel.toDetailsPresentationModel() = PokemonDetailsPresentationModel(
    height = height,
    weight = weight,
    name = name,
    imageUri = sprites.other.officialArtwork.frontDefault
)
