package it.secci.pokemon.pokemondetails.model.ui

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PokemonDetailsPresentationModel(
    val height: Long,
    val weight: Long,
    val imageUri: String,
    val name: String
): Parcelable