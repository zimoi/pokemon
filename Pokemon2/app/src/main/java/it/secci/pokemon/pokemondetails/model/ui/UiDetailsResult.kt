package it.secci.pokemon.pokemondetails.model.ui

import android.os.Parcelable
import it.secci.pokemon.core.model.ui.LoadingStatus
import kotlinx.parcelize.Parcelize

@Parcelize
data class UiDetailsResult(
    var loadingStatus: LoadingStatus? = null,
    var uiDetails: PokemonDetailsPresentationModel? = null
) : Parcelable