package it.secci.pokemon.pokemondetails.usecase

import it.secci.pokemon.core.repository.Repository
import it.secci.pokemon.pokemondetails.model.network.PokemonDetailsNetworkModel
import javax.inject.Inject


class GetDetailsUseCase @Inject constructor(
    val repository: Repository
) {

    suspend operator fun invoke(detailsUrl: String): PokemonDetailsNetworkModel =
        repository.getPokemonDetails(detailsUrl)
}