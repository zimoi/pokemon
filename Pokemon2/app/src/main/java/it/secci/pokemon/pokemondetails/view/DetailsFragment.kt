package it.secci.pokemon.pokemondetails.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import it.secci.pokemon.core.model.ui.LoadingStatus
import it.secci.pokemon.databinding.FragmentDetailsBinding
import it.secci.pokemon.pokemondetails.viewmodel.DetailsViewModel
import it.secci.pokemon.pokemondetails.viewmodel.DetailsViewModelProvider
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class DetailsFragment : Fragment() {

    private var _binding: FragmentDetailsBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var detailsViewModelFactory: DetailsViewModelProvider.Factory

    private val detailsViewModel: DetailsViewModel by viewModels {
        DetailsViewModelProvider(
            assistedFactory = detailsViewModelFactory,
            owner = this@DetailsFragment,
            detailsUrl = DetailsFragmentArgs.fromBundle(requireArguments()).detailsUrl
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Timber.d("oncreateview")
        _binding = FragmentDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Timber.d("onviewcreated")

        super.onViewCreated(view, savedInstanceState)

        detailsViewModel.uiDetailsResultLiveData
            .observe(viewLifecycleOwner, Observer { detailsViewModelResult ->
                when (detailsViewModelResult.loadingStatus) {
                    LoadingStatus.OK -> {
                        Timber.d("details loading completed")
                        Timber.d(detailsViewModelResult.uiDetails.toString())
                        binding.textViewDetailsValueName.text =
                            detailsViewModelResult.uiDetails?.name
                        binding.textViewDetailsValueWeight.text =
                            detailsViewModelResult.uiDetails?.weight.toString()
                        binding.textViewDetailsValueHeight.text =
                            detailsViewModelResult.uiDetails?.height.toString()
                        val imageUri = detailsViewModelResult.uiDetails?.imageUri
                        Glide.with(this)
                            .load(imageUri)
                            .into(binding.imageViewDetails)
                    }
                    LoadingStatus.KO -> {
                        Timber.e("Unable to load details :(")
                    }
                    LoadingStatus.LOADING -> {
                        Timber.d("details loading completed :)")
                    }

                    null -> Timber.d("loading status is null?!")
                }
            })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}