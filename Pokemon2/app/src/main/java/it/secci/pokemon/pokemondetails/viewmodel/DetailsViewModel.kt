package it.secci.pokemon.pokemondetails.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import it.secci.pokemon.core.model.ui.LoadingStatus
import it.secci.pokemon.pokemondetails.model.network.toDetailsPresentationModel
import it.secci.pokemon.pokemondetails.model.ui.PokemonDetailsPresentationModel
import it.secci.pokemon.pokemondetails.model.ui.UiDetailsResult
import it.secci.pokemon.pokemondetails.usecase.GetDetailsUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class DetailsViewModel @AssistedInject constructor(
    private val getDetailsUseCase: GetDetailsUseCase,
    private val defaultDispatcher: CoroutineDispatcher,
    @Assisted private val detailsUrl: String?,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {


    var uiDetailsResultLiveData: MutableLiveData<UiDetailsResult> =
        savedStateHandle.getLiveData(KEY)

    init {
        Timber.d("init DetailsViewModel")
        val savedData = savedStateHandle.get<UiDetailsResult>(KEY)
        if (savedData == null) {
            getDetails()
        }
    }

    private fun getDetails(detailsUrl: String? = this.detailsUrl)  {
        viewModelScope.launch {
            uiDetailsResultLiveData.postValue(
                UiDetailsResult(loadingStatus = LoadingStatus.LOADING)
            )

            try {
                val uiDetails = withContext(defaultDispatcher) {
                    return@withContext detailsUrl?.let { getDetailsUseCase(it).toDetailsPresentationModel() }
                }
                savedDetails(uiDetails)

            } catch (e: Exception) {
                Timber.e(e, "Error loading data")
                uiDetailsResultLiveData.postValue(
                    UiDetailsResult(
                        loadingStatus = LoadingStatus.KO
                    )
                )
            }
        }
    }

    private fun savedDetails(details: PokemonDetailsPresentationModel?) {
        Timber.d("saveDetails called")
        savedStateHandle[KEY] = UiDetailsResult(
            loadingStatus = LoadingStatus.OK,
            details
        )
    }

    companion object {
        const val KEY = "details"
    }

}