package it.secci.pokemon.pokemondetails.viewmodel

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import dagger.assisted.AssistedFactory

class DetailsViewModelProvider(
    owner: SavedStateRegistryOwner,
    defaultArgs: Bundle? = null,
    private val assistedFactory: Factory,
    private val detailsUrl: String?
) : AbstractSavedStateViewModelFactory(owner, defaultArgs) {

    @AssistedFactory
    interface Factory {
        fun create(handle: SavedStateHandle, detailsUrl: String?): DetailsViewModel
    }

    override fun <T : ViewModel> create(
        key: String,
        modelClass: Class<T>,
        handle: SavedStateHandle
    ): T {
        return assistedFactory.create(handle, detailsUrl) as T
    }
}