package it.secci.pokemon.pokemonlist.model.network

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

@Parcelize
data class PokemonListResponseNetworkModel (
    @field:Json(name = "count") var count: Long,
    @field:Json(name = "next") var next: String,
    @field:Json(name = "previous") var previous: String,
    @field:Json(name = "results") var pokemonLiteList: List<PokemonLite>
): Parcelable

@Parcelize
data class PokemonLite (
    @field:Json(name = "name") var name: String,
    @field:Json(name = "url") var url: String
): Parcelable