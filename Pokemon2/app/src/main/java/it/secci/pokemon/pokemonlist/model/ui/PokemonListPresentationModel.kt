package it.secci.pokemon.pokemonlist.model.ui

import android.os.Parcelable
import it.secci.pokemon.core.model.ui.LoadingStatus
import it.secci.pokemon.pokemonlist.model.network.PokemonLite
import kotlinx.parcelize.Parcelize

@Parcelize
data class PokemonListPresentationModel(
    var loadingStatus: LoadingStatus,
    var pokemonList: List<PokemonLite> = emptyList()
) : Parcelable