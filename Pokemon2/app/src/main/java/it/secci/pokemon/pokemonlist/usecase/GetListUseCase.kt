package it.secci.pokemon.pokemonlist.usecase

import it.secci.pokemon.core.repository.Repository
import it.secci.pokemon.pokemonlist.model.network.PokemonListResponseNetworkModel
import javax.inject.Inject

class GetListUseCase @Inject constructor(
    val repository: Repository
) {

    suspend operator fun invoke(): PokemonListResponseNetworkModel =
        repository.getPokemonList()
}