package it.secci.pokemon.pokemonlist.usecase

import it.secci.pokemon.core.repository.Repository
import it.secci.pokemon.pokemonlist.model.network.PokemonListResponseNetworkModel
import javax.inject.Inject

class GetNextPageUseCase @Inject constructor(
    val repository: Repository
) {

    suspend operator fun invoke(nextPageUrl: String): PokemonListResponseNetworkModel =
        repository.downloadAnotherPage(nextPageUrl)
}