package it.secci.pokemon.pokemonlist.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.AndroidEntryPoint
import it.secci.pokemon.core.model.ui.LoadingStatus
import it.secci.pokemon.databinding.FragmentListBinding
import it.secci.pokemon.pokemonlist.view.adapter.RecyclerViewAdapter
import it.secci.pokemon.pokemonlist.viewmodel.ListViewModel
import it.secci.pokemon.pokemonlist.viewmodel.ListViewModelProvider
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class ListFragment : Fragment() {

    private var _binding: FragmentListBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var listViewModelFactory: ListViewModelProvider.Factory

    val listViewModel: ListViewModel by viewModels {
        ListViewModelProvider(assistedFactory = listViewModelFactory, owner = this@ListFragment)
    }

    private lateinit var recyclerView: RecyclerView
    private lateinit var listAdapter: RecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Timber.d("onCreateView")
        _binding = FragmentListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.d("onViewCreated")

        recyclerView = createRecyclerView()
        listAdapter = createAdapter()
        recyclerView.adapter = listAdapter

        listViewModel.pokemonListPresentationModelLiveData.observe(
            viewLifecycleOwner, Observer { uiPokemonListResult ->
                when (uiPokemonListResult.loadingStatus) {
                    LoadingStatus.OK -> {
                        Timber.d("loading ok")
                        listAdapter.enqueueList(uiPokemonListResult.pokemonList)
                    }
                    LoadingStatus.LOADING -> Timber.d("loading in progress")
                    LoadingStatus.KO -> Timber.d("loading KO")
                }
            }
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun createRecyclerView(): RecyclerView {
        val recyclerView = binding.recyclerViewPokemonList
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (isBottomReached(newState)) {
                    Timber.d("end of recyclerview reached")
                    listViewModel.onBottomListReached()
                }
            }
        })

        return recyclerView
    }

    private fun createAdapter(): RecyclerViewAdapter {
        return RecyclerViewAdapter { pokemon , position ->
            Toast.makeText(
                requireActivity(),
                "${pokemon.name}, scelgo te! ($position)",
                Toast.LENGTH_SHORT
            ).show()
            val actionToPokemonDetailsFragment =
                ListFragmentDirections.actionListFragmentToDetailsFragment(
                    pokemon.url
                )
            findNavController().navigate(actionToPokemonDetailsFragment)
        }
    }

    private fun isBottomReached(newState: Int): Boolean =
        !recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE
}