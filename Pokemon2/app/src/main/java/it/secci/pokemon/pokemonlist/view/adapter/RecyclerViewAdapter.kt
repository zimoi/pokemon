package it.secci.pokemon.pokemonlist.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import it.secci.pokemon.databinding.ItemListBinding
import it.secci.pokemon.pokemonlist.model.network.PokemonLite

class RecyclerViewAdapter(
    private val onItemClicked: (pokemon: PokemonLite, position: Int) -> Unit
) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    private var pokemonList: List<PokemonLite> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemListBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false
        ))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position).let { pokemon ->
            holder.bind(pokemon, onItemClicked)
        }
    }

    class ViewHolder(private val itemListBinding: ItemListBinding) : RecyclerView.ViewHolder(itemListBinding.root) {
        fun bind(pokemon: PokemonLite, onItemClicked: (pokemon: PokemonLite, position: Int) -> Unit) {
            itemListBinding.constraintLayoutItemContainer.setOnClickListener {
                onItemClicked(pokemon, bindingAdapterPosition + 1)
            }
            itemListBinding.textViewItemNumber.text = (bindingAdapterPosition + 1).toString()
            itemListBinding.textViewName.text = pokemon.name
            itemListBinding.textViewUrl.text = pokemon.url
        }
    }

    override fun getItemCount(): Int {
        return pokemonList.size
    }

    fun enqueueList(pokemonList: List<PokemonLite>) {
        this.pokemonList = pokemonList
        notifyDataSetChanged() //todo rimuovere - vedi https://blog.mindorks.com/the-powerful-tool-diff-util-in-recyclerview-android-tutorial
//        notifyItemRangeInserted(this.pokemonList.size, pokemonList.size)
    }

    private fun getItem(position: Int): PokemonLite {
        return pokemonList[position]
    }
}