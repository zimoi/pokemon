package it.secci.pokemon.pokemonlist.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import it.secci.pokemon.core.model.ui.LoadingStatus
import it.secci.pokemon.pokemonlist.model.network.PokemonLite
import it.secci.pokemon.pokemonlist.model.ui.PokemonListPresentationModel
import it.secci.pokemon.pokemonlist.usecase.GetListUseCase
import it.secci.pokemon.pokemonlist.usecase.GetNextPageUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class ListViewModel @AssistedInject constructor(
    val getListUseCase: GetListUseCase,
    val getNextPageUseCase: GetNextPageUseCase,
    private val defaultDispatcher: CoroutineDispatcher,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private var pokemonList: MutableList<PokemonLite> = mutableListOf()
    private var nextPage: String? = null
    private var previousPage: String? = null

    var pokemonListPresentationModelLiveData: MutableLiveData<PokemonListPresentationModel> =
        savedStateHandle.getLiveData(KEY_MODEL)

    init {
        Timber.d("init ListViewModel")
        val savedData = savedStateHandle.get<PokemonListPresentationModel>(KEY_MODEL)
        if (savedData == null) {
            updateList()
        } else {
            pokemonList.addAll(savedData.pokemonList)
            nextPage = savedStateHandle.get<String?>(KEY_NEXT_PAGE)
            previousPage = savedStateHandle.get<String?>(KEY_PREVIOUS_PAGE)
        }
    }

    fun onBottomListReached() {
        updateList()
    }

    private fun updateList() {
        Timber.d("UpdateList called")

        viewModelScope.launch {
            pokemonListPresentationModelLiveData.postValue(
                PokemonListPresentationModel(loadingStatus = LoadingStatus.LOADING)
            )

            try {
                withContext(defaultDispatcher) {
                    val pokemonListResponse = nextPage?.let {
                        getNextPageUseCase(it)
                    } ?: getListUseCase()
                    nextPage = pokemonListResponse.next
                    previousPage = pokemonListResponse.previous
                    pokemonList.addAll(pokemonListResponse.pokemonLiteList)
                }
                saveListLiveData(pokemonList, nextPage, previousPage)

            } catch (e: Exception) {
                Timber.e("Error loading data", e)
                pokemonListPresentationModelLiveData.postValue(
                    PokemonListPresentationModel(loadingStatus = LoadingStatus.KO)
                )
            }
        }

    }

    private fun saveListLiveData(pokemonList: List<PokemonLite>, nextPage: String?, previousPage: String?) {
        Timber.d("saveLiveData called")
        savedStateHandle[KEY_MODEL] = PokemonListPresentationModel(
            loadingStatus = LoadingStatus.OK,
            pokemonList
        )
        savedStateHandle[KEY_NEXT_PAGE] = nextPage
        savedStateHandle[KEY_PREVIOUS_PAGE] = previousPage
    }

    companion object {
        const val KEY_MODEL = "list"
        const val KEY_NEXT_PAGE = "next_page"
        const val KEY_PREVIOUS_PAGE = "previous_page"
    }
}