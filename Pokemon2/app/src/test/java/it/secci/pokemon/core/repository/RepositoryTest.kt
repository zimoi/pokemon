package it.secci.pokemon.core.repository

import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers
import org.hamcrest.core.Every.everyItem
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

import org.hamcrest.Matchers.hasProperty

class RepositoryTest {

    lateinit var pokemonRepository : Repository

//    @get:Rule
//    val koinTestRule = KoinTestRule.create {
//        modules(module {
//            single<PokemonRepository> { PokemonRepositoryImpl(get()) }
//            factory { Retrofit.Builder().baseUrl(Urls.POKEMON_BASE_URL)
//                .client(OkHttpClient.Builder().build())
//                .addConverterFactory(MoshiConverterFactory.create())
//                .build().create(PokemonService::class.java)
//            }
//        })
//    }

    @Test
    fun firstDownload_isWorking(): Unit = runBlocking {

        val pokemonListResponse = pokemonRepository.getPokemonList()
        pokemonListResponse.pokemonLiteList?.forEach {
            println(it)
        }
        assertThat(
            pokemonListResponse.pokemonLiteList,
            everyItem(hasProperty("name", CoreMatchers.notNullValue()))
        )
        assertThat(
            pokemonListResponse.pokemonLiteList,
            everyItem(hasProperty("url", CoreMatchers.notNullValue()))
        )
    }

    @Test
    fun nextPage_isReachable(): Unit = runBlocking {
        var pokemonListResponse = pokemonRepository.getPokemonList()
        pokemonListResponse = pokemonRepository.downloadAnotherPage(pokemonListResponse.next!!)
        pokemonListResponse.pokemonLiteList?.forEach {
            println(it)
        }

        assertThat(
            pokemonListResponse.pokemonLiteList,
            everyItem(hasProperty("name", CoreMatchers.notNullValue()))
        )
        assertThat(
            pokemonListResponse.pokemonLiteList,
            everyItem(hasProperty("url", CoreMatchers.notNullValue()))
        )
    }

    @Test
    fun pokemonDetails_isWorking(): Unit = runBlocking {
        val pokemonDetails = pokemonRepository.getPokemonDetails("https://pokeapi.co/api/v2/pokemon/24/")
        println(pokemonDetails)

        assertThat(pokemonDetails, CoreMatchers.notNullValue())
    }
}